using Microsoft.AspNetCore.Mvc;
using Dotnet8CRUD.Data;
using Dotnet8CRUD.Models;
using Microsoft.EntityFrameworkCore;
using Services;
using System.Security.Claims;

namespace Controllers;

public class AuthController(ApplicationDbContext context, ITokenService service) : Controller
{
    private readonly ApplicationDbContext _context = context;
    private readonly ITokenService _service = service;
    public IActionResult Login()
    {
        return View();
    }

    public IActionResult Register()
    {
        return View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Register([Bind("Username,Password")] User user)
    {
        Console.WriteLine(ModelState.IsValid);
        if (ModelState.IsValid)
        {
            user.CreatedAt = DateTime.Now;
            user.Guid = Guid.NewGuid().ToString();
            user.Password = BCrypt.Net.BCrypt.HashPassword(user.Password);
            _context.Add(user);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Login));
        }
        return View(user);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Login([Bind("Username,Password")] User user)
    {
        var _user = await _context.User.FirstOrDefaultAsync(u => u.Username == user.Username);
        if (_user != null && BCrypt.Net.BCrypt.Verify(user.Password, _user.Password))
        {
            var jwtToken = _service.CreateToken(_user);

            Response.Cookies.Append("jwt", jwtToken, new CookieOptions
            {
                HttpOnly = true
            });
            return RedirectToAction("Index", "Home");
        }
        return View(user);
    }

    public IActionResult Logout()
    {
        Response.Cookies.Delete("jwt");
        return RedirectToAction("Index", "Home");
    }
}