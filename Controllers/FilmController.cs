using System.Security.Claims;
using Dotnet8CRUD.Data;
using Dotnet8CRUD.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Controllers;


public class FIlmController(ApplicationDbContext context) : Controller
{
    private readonly ApplicationDbContext _context = context;

    [Authorize]
    public async Task<IActionResult> Index()
    {
        if (Response.StatusCode == 401) return RedirectToAction("Login", "Auth");

        if (!User.Identity.IsAuthenticated)
        {
            return RedirectToAction("Login", "Auth");
        }
        // join with genre table
        var _Film = await _context.Film
                                  .Include(f => f.Genre) 
                                  .ToListAsync();
        return _Film != null ?
                    View(_Film) :
                    Problem("Entity set 'ApplicationDbContext.Film'  is null.");
    }

    [Authorize]
    public IActionResult Create()
    {
        return View();
    }

    [Authorize]
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("Id,Title,GenreId,Director,Year")] Film film)
    {
        if (ModelState.IsValid)
        {
            film.CreatedAt = DateTime.Now;
            _context.Add(film);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        return View(film);
    }

    [Authorize]
    public async Task<IActionResult> Details(int? id)
    {
        if (id == null || _context.Film == null)
        {
            return NotFound();
        }

        var film = await _context.Film
            .Include(f => f.Genre)
            .FirstOrDefaultAsync(m => m.Id == id);
        if (film == null)
        {
            return NotFound();
        }

        return View(film);
    }

    [Authorize]
    public async Task<IActionResult> Edit(int? id)
    {
        if (id == null || _context.Film == null)
        {
            return NotFound();
        }

        var film = await _context.Film.Include(f => f.Genre).FirstOrDefaultAsync(m => m.Id == id);
        if (film == null)
        {
            return NotFound();
        }

        return View(film);
    }

    [Authorize]
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(int id, [Bind("Id,Title,GenreId,Director,Year")] Film film)
    {
        if (id != film.Id)
        {
            return NotFound();
        }

        if (ModelState.IsValid)
        {
            try
            {
                film.UpdatedAt = DateTime.Now;
                _context.Update(film);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }
            return RedirectToAction(nameof(Index));
        }
        return View(film);
    }


    [Authorize]
    public async Task<JsonResult> Delete(int id)
    {
        try
        {
            var _film = await _context.Film.FindAsync(id);

            if (_film != null)
            {
                _context.Film.Remove(_film);
            }
            await _context.SaveChangesAsync();
            return new JsonResult(_film);
        }
        catch (Exception)
        {
            throw;
        }
    }
}