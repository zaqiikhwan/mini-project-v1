using Dotnet8CRUD.Models;
using Microsoft.EntityFrameworkCore;

namespace Dotnet8CRUD.Data
{
    public class ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : DbContext(options)
    {
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Genre>().HasData(
                new Genre { Id = 1, Name = "Action" },
                new Genre { Id = 2, Name = "Comedy" },
                new Genre { Id = 3, Name = "Drama" },
                new Genre { Id = 4, Name = "Fantasy" },
                new Genre { Id = 5, Name = "Horror" },
                new Genre { Id = 6, Name = "Mystery" },
                new Genre { Id = 7, Name = "Romance" },
                new Genre { Id = 8, Name = "Thriller" }
            );

            // seed data film

            builder.Entity<Film>().HasData(
                new Film { Id = 1, Title = "The Shawshank Redemption", GenreId = 3, Director = "Frank Darabont", Year = 1994 },
                new Film { Id = 2, Title = "The Godfather", GenreId = 3, Director = "Francis Ford Coppola", Year = 1972 },
                new Film { Id = 3, Title = "The Dark Knight", GenreId = 1, Director = "Christopher Nolan", Year = 2008 },
                new Film { Id = 4, Title = "The Godfather: Part II", GenreId = 3, Director = "Francis Ford Coppola", Year = 1974 },
                new Film { Id = 5, Title = "Pulp Fiction", GenreId = 7, Director = "Quentin Tarantino", Year = 1994 },
                new Film { Id = 6, Title = "The Lord of the Rings: The Return of the King", GenreId = 4, Director = "Peter Jackson", Year = 2003 },
                new Film { Id = 7, Title = "Forrest Gump", GenreId = 7, Director = "Robert Zemeckis", Year = 1994 },
                new Film { Id = 8, Title = "Inception", GenreId = 6, Director = "Christopher Nolan", Year = 2010 },
                new Film { Id = 9, Title = "The Matrix", GenreId = 1, Director = "Lana Wachowski, Lilly Wachowski", Year = 1999 },
                new Film { Id = 10, Title = "The Silence of the Lambs", GenreId = 8, Director = "Jonathan Demme", Year = 1991 },
                new Film { Id = 11, Title = "Schindler's List", GenreId = 3, Director = "Steven Spielberg", Year = 1993 },
                new Film { Id = 12, Title = "Inglourious Basterds", GenreId = 1, Director = "Quentin Tarantino", Year = 2009 },
                new Film { Id = 13, Title = "The Shining", GenreId = 5, Director = "Stanley Kubrick", Year = 1980 },
                new Film { Id = 14, Title = "Titanic", GenreId = 7, Director = "James Cameron", Year = 1997 },
                new Film { Id = 15, Title = "The Departed", GenreId = 8, Director = "Martin Scorsese", Year = 2006 },
                new Film { Id = 16, Title = "Jurassic Park", GenreId = 4, Director = "Steven Spielberg", Year = 1993 },
                new Film { Id = 17, Title = "The Lion King", GenreId = 4, Director = "Roger Allers, Rob Minkoff", Year = 1994 },
                new Film { Id = 18, Title = "Gone with the Wind", GenreId = 3, Director = "Victor Fleming", Year = 1939 },
                new Film { Id = 19, Title = "Avatar", GenreId = 4, Director = "James Cameron", Year = 2009 },
                new Film { Id = 20, Title = "Fight Club", GenreId = 1, Director = "David Fincher", Year = 1999 }
            );

        }

        public DbSet<Category> Category { get; set; }
        public DbSet<Film> Film { get; set; }

        public DbSet<Genre> Genre { get; set; }

        public DbSet<User> User { get; set; }
    }
}
