using System.ComponentModel.DataAnnotations;

namespace Dotnet8CRUD.Models
{
    public class Genre
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        public List<Film> Film {get; set;}
    }
}