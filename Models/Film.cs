using System.ComponentModel.DataAnnotations;

namespace Dotnet8CRUD.Models
{
    public class Film
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public int GenreId { get; set;}

        public Genre Genre {get; set;} = null;
        public string Director { get; set; }
        public int Year { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}