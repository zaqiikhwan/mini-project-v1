using System.ComponentModel.DataAnnotations;

namespace Dotnet8CRUD.Models
{
    public class User
    {
        [Key]
        public string Guid { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}