﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Dotnet8CRUD.Migrations
{
    /// <inheritdoc />
    public partial class first_commit : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Category",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Category", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Genre",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Genre", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Guid = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Username = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Guid);
                });

            migrationBuilder.CreateTable(
                name: "Film",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    GenreId = table.Column<int>(type: "int", nullable: false),
                    Director = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Year = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Film", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Film_Genre_GenreId",
                        column: x => x.GenreId,
                        principalTable: "Genre",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Genre",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Action" },
                    { 2, "Comedy" },
                    { 3, "Drama" },
                    { 4, "Fantasy" },
                    { 5, "Horror" },
                    { 6, "Mystery" },
                    { 7, "Romance" },
                    { 8, "Thriller" }
                });

            migrationBuilder.InsertData(
                table: "Film",
                columns: new[] { "Id", "CreatedAt", "Director", "GenreId", "Title", "UpdatedAt", "Year" },
                values: new object[,]
                {
                    { 1, new DateTime(2024, 3, 1, 3, 17, 41, 86, DateTimeKind.Local).AddTicks(9907), "Frank Darabont", 3, "The Shawshank Redemption", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1994 },
                    { 2, new DateTime(2024, 3, 1, 3, 17, 41, 86, DateTimeKind.Local).AddTicks(9923), "Francis Ford Coppola", 3, "The Godfather", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1972 },
                    { 3, new DateTime(2024, 3, 1, 3, 17, 41, 86, DateTimeKind.Local).AddTicks(9924), "Christopher Nolan", 1, "The Dark Knight", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 2008 },
                    { 4, new DateTime(2024, 3, 1, 3, 17, 41, 86, DateTimeKind.Local).AddTicks(9925), "Francis Ford Coppola", 3, "The Godfather: Part II", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1974 },
                    { 5, new DateTime(2024, 3, 1, 3, 17, 41, 86, DateTimeKind.Local).AddTicks(9926), "Quentin Tarantino", 7, "Pulp Fiction", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1994 },
                    { 6, new DateTime(2024, 3, 1, 3, 17, 41, 86, DateTimeKind.Local).AddTicks(9927), "Peter Jackson", 4, "The Lord of the Rings: The Return of the King", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 2003 },
                    { 7, new DateTime(2024, 3, 1, 3, 17, 41, 86, DateTimeKind.Local).AddTicks(9928), "Robert Zemeckis", 7, "Forrest Gump", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1994 },
                    { 8, new DateTime(2024, 3, 1, 3, 17, 41, 86, DateTimeKind.Local).AddTicks(9929), "Christopher Nolan", 6, "Inception", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 2010 },
                    { 9, new DateTime(2024, 3, 1, 3, 17, 41, 86, DateTimeKind.Local).AddTicks(9931), "Lana Wachowski, Lilly Wachowski", 1, "The Matrix", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1999 },
                    { 10, new DateTime(2024, 3, 1, 3, 17, 41, 86, DateTimeKind.Local).AddTicks(9932), "Jonathan Demme", 8, "The Silence of the Lambs", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1991 },
                    { 11, new DateTime(2024, 3, 1, 3, 17, 41, 86, DateTimeKind.Local).AddTicks(9933), "Steven Spielberg", 3, "Schindler's List", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1993 },
                    { 12, new DateTime(2024, 3, 1, 3, 17, 41, 86, DateTimeKind.Local).AddTicks(9972), "Quentin Tarantino", 1, "Inglourious Basterds", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 2009 },
                    { 13, new DateTime(2024, 3, 1, 3, 17, 41, 86, DateTimeKind.Local).AddTicks(9974), "Stanley Kubrick", 5, "The Shining", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1980 },
                    { 14, new DateTime(2024, 3, 1, 3, 17, 41, 86, DateTimeKind.Local).AddTicks(9975), "James Cameron", 7, "Titanic", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1997 },
                    { 15, new DateTime(2024, 3, 1, 3, 17, 41, 86, DateTimeKind.Local).AddTicks(9976), "Martin Scorsese", 8, "The Departed", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 2006 },
                    { 16, new DateTime(2024, 3, 1, 3, 17, 41, 86, DateTimeKind.Local).AddTicks(9977), "Steven Spielberg", 4, "Jurassic Park", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1993 },
                    { 17, new DateTime(2024, 3, 1, 3, 17, 41, 86, DateTimeKind.Local).AddTicks(9978), "Roger Allers, Rob Minkoff", 4, "The Lion King", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1994 },
                    { 18, new DateTime(2024, 3, 1, 3, 17, 41, 86, DateTimeKind.Local).AddTicks(9979), "Victor Fleming", 3, "Gone with the Wind", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1939 },
                    { 19, new DateTime(2024, 3, 1, 3, 17, 41, 86, DateTimeKind.Local).AddTicks(9980), "James Cameron", 4, "Avatar", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 2009 },
                    { 20, new DateTime(2024, 3, 1, 3, 17, 41, 86, DateTimeKind.Local).AddTicks(9981), "David Fincher", 1, "Fight Club", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1999 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Film_GenreId",
                table: "Film",
                column: "GenreId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Category");

            migrationBuilder.DropTable(
                name: "Film");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropTable(
                name: "Genre");
        }
    }
}
