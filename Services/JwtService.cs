using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Dotnet8CRUD.Models;
using Microsoft.IdentityModel.Tokens;

namespace Services
{
    public interface ITokenService
    {
        string CreateToken(User user);
    }
    public class TokenService(IConfiguration config) : ITokenService
    {
        private readonly SymmetricSecurityKey _key = new(Encoding.UTF8.GetBytes(config?["TokenKey"]));
        public string CreateToken(User user)
        {
            Console.WriteLine(config["TokenKey"]);
            Console.WriteLine(_key.ToString());
            var claims = new List<Claim>
            {
                new(JwtRegisteredClaimNames.Name, user.Username)
            };

            var creds = new SigningCredentials(_key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Claims = new Dictionary<string, object>
                {
                    {"guid", user.Guid}
                },
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddHours(1),
                SigningCredentials = creds,
            };
            var tokenHandler = new JwtSecurityTokenHandler();

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }


    }
}
